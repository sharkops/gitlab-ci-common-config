# Docker CI Config

This folder contains some GitLab CI config files that you can include in your project in order to work with Docker Images.

* `build.yml`: This will build a Docker Image using Google's Kaniko builder (in order to avoid needing Docker-in-Docker (DIND)). Every build that is not a tag will be tagged with the $CI_COMMIT_SHORT_SHA and the $CI_COMMIT_REF_NAME (branch name). Tag builds will be tagged with $CI_COMMIT_TAG and `latest`.

## Examples

Building a docker Image and use GitLab Container Registry:

```yaml
include:
  - project: 'sharkops/gitlab-ci-common-config'
    file: 'docker/build.yml'

variables:
  REGISTRY_URL: $CI_REGISTRY
  REGISTRY_USER: $CI_REGISTRY_USER
  REGISTRY_PASSWORD: $CI_REGISTRY_PASSWORD
  IMAGE_NAME: $CI_REGISTRY_IMAGE
```

Building a docker Image and use Docker Hub:

```yaml
include:
  - project: 'sharkops/gitlab-ci-common-config'
    file: 'docker/build.yml'

variables:
  REGISTRY_URL: docker.io
  REGISTRY_USER: <user> (Store in project secret config)
  REGISTRY_PASSWORD: <password> (Store in project secret config)
  IMAGE_NAME: $REGISTRY_URL/user/image-name
```
