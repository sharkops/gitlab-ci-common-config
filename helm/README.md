# Helm CI Config

This folder contains some GitLab CI config files that you can include in your project in order to work with Helm Charts.

* `build.yml`: This will build a Helm Chart and host a Helm Repository on GitLab Pages. In order to start the Pipeline, create a new git `tag` for your repository. Do not create a GitLab Release as this config will do that for you based on your Tag.

## Examples

### Deploying your Helm Chart to GitLab Pages:

```yaml
include:
  - project: 'sharkops/gitlab-ci-common-config'
    file: 'helm/build.yml'
```

Using your Helm Chart repo:

Please see https://docs.gitlab.com/ee/user/project/pages/getting_started_part_one.html for URL examples.

```bash
helm repo add mychart https://sharkops.gitlab.io/helm-charts/cluster-tools
helm repo update
```
