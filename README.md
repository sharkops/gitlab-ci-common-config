# GitLab CI Common Config

This repository is a collection of common abd reusable GitLab CI config snippets. They can be included in any GitLab CI/CD configuration. We will make sure to maintain versioned tags of this repository so you can rely on them to not change. If you like to live on the edge, you can omit a version and just use our `master` branch.

## Contents

* [Docker](docker/)
* [Helm](helm/)


## Including a Docker build snippet, targeting the 1.0.0 tag

```yaml
include:
  - project: 'sharkops/gitlab-ci-common-config'
    file: 'docker/build.yml'
    ref: '1.0.0'
```

## Including a Docker build snippet, targeting "latest"

```yaml
include:
  - project: 'sharkops/gitlab-ci-common-config'
    file: 'docker/build.yml'
```
